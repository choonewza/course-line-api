const express = require("express");
const router = express.Router();
const { line, config } = require("../config/line");
const { handleEvent } = require("../services/handle-event");

//http://localhost:4000/line/callback
router.post("/callback", line.middleware(config),  function (req, res, next) {
  // console.log("ReqBody : ", req.body);
  Promise.all(req.body.events.map(handleEvent))
    .then((result) => res.json(result))
    .catch((err) => {
      console.error(err);
      res.status(500).end();
    });
});

module.exports = router;
