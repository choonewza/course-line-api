const express = require("express");
const router = express.Router();
const db = require("../config/mysql");
const { createRichMenu } = require("../services/create-richmenu");
const { deleteRichMenu } = require("../services/delete-richmenu");
const { client } = require("../config/line");
const { default: axios } = require("axios");
const jwt_decode = require("jwt-decode");

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "Express" });
});

// localhost:4000/api/promotions
router.get("/api/promotion", async function (req, res, next) {
  const conn = await db.connectMySQL();
  const [rows] = await conn.query(
    "SELECT * FROM pro_net ORDER BY id DESC LIMIT 12"
  );

  return res.status(200).json(rows);
});

// localhost:4000/api/promotions/2
router.get("/api/promotion/:id", async function (req, res, next) {
  const { id } = req.params;
  const conn = await db.connectMySQL();
  const [rows] = await conn.query("SELECT * FROM pro_net WHERE id =?", [id]);

  return res.status(200).json(rows.length > 0 ? rows[0] : null);
});

// create richmenu aa
// localhost:4000/createrichmenu
router.get("/createrichmenu", async function (req, res, next) {
  await createRichMenu();
  return res.status(200).json({ message: "สร้าง rich menu สำเร็จ" });
});

// delete richmenu aa
// localhost:4000/deleterichmenu
router.get("/deleterichmenu", async function (req, res, next) {
  await deleteRichMenu();
  return res.status(200).json({ message: "ลบ rich menu สำเร็จ" });
});

// send push message
// localhost:4000/sendtouser
router.get("/sendtouser/:lineId", async function (req, res, next) {
  try {
    let msg2 = { type: "text", text: "สวัสดี ข้อความนี้เป็นการ push มา" };
    await client.pushMessage(req.params.lineId, msg2);
    return res.status(200).json({ message: "ส่งข้อความ Push สำเร็จ" });
  } catch (ex) {
    console.log(ex);
    return res.status(200).json({ message: "ส่งข้อความ Push ไม่สำเร็จ" });
  }
});

// send broadcast message
// localhost:4000/sendtobroadcast
router.get("/sendtobroadcast", async function (req, res, next) {
  try {
    let msg3 = { type: "text", text: "สวัสดี ข้อความนี้เป็นการ broadcast มา" };
    await client.broadcast(msg3);
    return res.status(200).json({ message: "ส่งข้อความ Broadcast สำเร็จ" });
  } catch (ex) {
    console.log(ex);
    return res.status(200).json({ message: "ส่งข้อความ Broadcast ไม่สำเร็จ" });
  }
});

// LINE Login callback url
// localhost:4000/auth/callback
// https://developers.line.biz/en/docs/line-login/integrate-line-login/#making-an-authorization-request
router.get("/auth/callback", async function (req, res, next) {
  console.log(req.query.code);

  // get access token and profile
  const params = new URLSearchParams({
    grant_type: "authorization_code",
    code: req.query.code,
    redirect_uri: process.env.BASE_URL + "/auth/callback", //callback url (channel)
    client_id: process.env.LINE_LOGIN_CLIENT_ID, //channel id
    client_secret: process.env.LINE_LOGIN_CLIENT_SECRET, //channel secret
  });

  const response = await axios.post(process.env.LINE_LOGIN_URL, params, {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
  });

  const user = jwt_decode(response.data.id_token);

  return res.status(200).json({
    user: {
      id: user.sub,
      name: user.name,
      picture: user.picture,
      email: user.email,
    },
    access_token: response.data,
  });
});

module.exports = router;
