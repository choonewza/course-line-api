const { format, formatDistance } = require("date-fns");
const { th } = require("date-fns/locale");

exports.fromNow = (date) => {
  return formatDistance(new Date(date), new Date(), {
    addSuffix: true,
    locale: th,
  });
};

exports.thaiFormat = (date) => {
  const dayStr = format(new Date(date), "d MMM yyyy", { locale: th });
  return (
    dayStr.slice(0, - 4) +
    (parseInt(dayStr.substring(8)) + 543)
  );
};
