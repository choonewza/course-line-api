const numeral = require("numeral");

exports.numberFormat = (num) => {
  return numeral(num).format("0,0");
};
