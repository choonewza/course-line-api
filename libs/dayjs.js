require("dayjs/locale/th");

const dayjs = require("dayjs");
const relativeTime = require("dayjs/plugin/relativeTime");
const buddhistEra = require("dayjs/plugin/buddhistEra");
dayjs.extend(buddhistEra);
dayjs.extend(relativeTime);
dayjs.locale("th");

exports.dayjs = dayjs;

exports.fromNow = (date) => {
  return dayjs(date).fromNow();
};

exports.thaiFormat = (date) => {
  return dayjs(date).format("DD MMM BBBB");
};
