const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");

const dotenv = require("dotenv");
dotenv.config();

const cors = require("cors");

const indexRouter = require("./routes/index")
const lineRouter = require("./routes/line")
const metaRouter = require("./routes/meta")

const app = express();

app.use(cors()); //First Check CORS....

app.use(logger("dev"));

//LINE API MUST ... Router must be BEFORE app.use(express.json());
app.use(express.static(path.join(__dirname, "public")));
app.use("/line", lineRouter); //http://localhost:4000/line
//------ END -------

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use("/", indexRouter); //http://localhost:4000/


app.use("/meta", metaRouter); // http://localhost:4000/meta

module.exports = app;
