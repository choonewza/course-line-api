/*
 * Image Message
 * https://developers.line.biz/en/reference/messaging-api/#image-message
 *
 */

exports.sendImage = () => {
  let msg;

  msg = {
    type: "image",
    originalContentUrl: process.env.BASE_URL + "/images/2.png",
    previewImageUrl: process.env.BASE_URL + "/images/1.png",
  };
  return msg;
};
