const { client } = require("../config/line");
const path = require("path");
const { readFileSync } = require("fs");

/*
 * Rich Menu
 * https://developers.line.biz/en/docs/messaging-api/using-rich-menus/
 * https://developers.line.biz/en/docs/messaging-api/using-rich-menus/
 * https://developers.line.biz/en/docs/messaging-api/try-rich-menu/#actions-you-can-try-out-on-the-richmenu-playground
 * https://developers.line.biz/en/docs/messaging-api/try-rich-menu/#actions-you-can-try-out-on-the-richmenu-playground
 */

exports.deleteRichMenu = async () => {
  await client.deleteRichMenuAlias("richmenu-alias-aa");
};
