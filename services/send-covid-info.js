/*
 * Image Message
 * https://developers.line.biz/en/reference/messaging-api/#image-message
 *
 */

const axios = require("axios").default;
const { fromNow, thaiFormat } = require("../libs/dateFns");
const {ç, numberFormat} = require("../libs/numeral");

const getCovicInfo = async () => {
  const response = await axios.get(
    "https://covid19.ddc.moph.go.th/api/Cases/today-cases-all",
    {
      headers: { "Content-Type": "application/json" },
    }
  );
  const data = response.data;
  return data[0];
};

exports.sendCovidInfo = async () => {
  let msg;

  const covidInfo = await getCovicInfo();

  msg = msg = {
    type: "flex",
    altText: "รายงานสถานการณ์โควิด ประจำวัน",
    contents: {
      type: "bubble",
      hero: {
        type: "image",
        url: process.env.BASE_URL + "/images/covid.jpeg",
        size: "full",
        aspectRatio: "20:13",
        aspectMode: "cover",
        action: {
          type: "uri",
          uri: "https://linecorp.com",
        },
      },
      body: {
        type: "box",
        layout: "vertical",
        spacing: "md",
        action: {
          type: "uri",
          uri: "https://covid19.ddc.moph.go.th/",
        },
        contents: [
          {
            type: "text",
            text: "วันที่ " + thaiFormat(covidInfo.txn_date),
            size: "xl",
            weight: "bold",
            color: "#2B870D",
          },
          {
            type: "box",
            layout: "vertical",
            spacing: "sm",
            contents: [
              {
                type: "box",
                layout: "baseline",
                contents: [
                  {
                    type: "text",
                    text: "ผู้ป่วยรายใหม่",
                    weight: "bold",
                    flex: 0,
                    size: "lg",
                    color: "#000000",
                  },
                  {
                    type: "text",
                    text: numberFormat(covidInfo.new_case) + " ราย",
                    size: "lg",
                    align: "end",
                    color: "#FC1E1E",
                  },
                ],
              },
              {
                type: "box",
                layout: "baseline",
                contents: [
                  {
                    type: "text",
                    text: "ผู้เสียชีวิตรายใหม่",
                    weight: "bold",
                    flex: 0,
                    size: "lg",
                    color: "#000000",
                  },
                  {
                    type: "text",
                    text: numberFormat(covidInfo.new_death) + " ราย",
                    size: "lg",
                    align: "end",
                    color: "#000000",
                  },
                ],
              },
            ],
          },
          {
            type: "text",
            text: "อัปเดต: " + fromNow(covidInfo.update_date),
            wrap: true,
            color: "#aaaaaa",
            size: "xxs",
          },
        ],
      },
      footer: {
        type: "box",
        layout: "vertical",
        contents: [
          {
            type: "button",
            style: "primary",
            color: "#905c44",
            margin: "xxl",
            action: {
              type: "uri",
              label: "ดูข้อมูลเพิ่มเติม...",
              uri: "https://ddc.moph.go.th/covid19-dashboard/",
            },
          },
        ],
      },
    },
  };

  return msg;
};
