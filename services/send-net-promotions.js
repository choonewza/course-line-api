/*
 * Image Message
 * https://developers.line.biz/en/reference/messaging-api/#image-message
 *
 */

const axios = require("axios").default;

const getNetPromotions = async () => {
  const response = await axios.get(process.env.BASE_URL + "/api/promotion", {
    headers: { "Content-Type": "application/json" },
  });
  const data = response.data;
  console.log("DDDD",data);
  return data;
};

exports.sendNetPromotions = async () => {
  let msg;

  const promotions = await getNetPromotions();
  console.log(promotions);

  const bubbles = promotions.map((row) => {
    const postbackType = { type: "promotion" };
    const postbackData = { ...postbackType, ...row };
    return {
      type: "bubble",
      hero: {
        type: "image",
        url: row.picture,
        size: "full",
        aspectRatio: "20:13",
        aspectMode: "cover",
        action: {
          type: "uri",
          uri: "https://linecorp.com",
        },
      },
      body: {
        type: "box",
        layout: "vertical",
        spacing: "md",
        action: {
          type: "uri",
          uri: "https://linecorp.com",
        },
        contents: [
          {
            type: "text",
            text: row.name,
            size: "xl",
            weight: "bold",
          },
        ],
      },
      footer: {
        type: "box",
        layout: "vertical",
        contents: [
          {
            type: "button",
            style: "primary",
            color: "#DFC010",
            margin: "xxl",
            action: {
              type: "postback",
              label: "ดูรายละเอียด",
              data: JSON.stringify(postbackData),
            },
          },
        ],
        paddingTop: "0px",
      },
    };
  });

  msg = {
    type: "flex",
    altText: "โปรโมชั่นยอดนิยม NT",
    contents: {
      type: "carousel",
      contents: bubbles,
    },
  };

  return msg;
};
