/*
 * Template Message
 * https://developers.line.biz/en/reference/messaging-api/#image-message
 *
 */

exports.sendTemplate = () => {
  let msg;

  msg = {
    "type": "template",
    "altText": "โปรโมชั่นเน็ตวันนี้",
    "template": {
      "type": "buttons",
      "thumbnailImageUrl": "https://picsum.photos/seed/picsum/1024/600",
      "imageAspectRatio": "rectangle",
      "imageSize": "cover",
      "imageBackgroundColor": "#FFFFFF",
      "title": "เน็ตเต็ม Speed",
      "text": "เลือกซื้อเลย!",
      "defaultAction": {
        "type": "uri",
        "label": "View detail",
        "uri": "http://example.com/page/123"
      },
      "actions": [
        {
          "type": "postback",
          "label": "Buy",
          "data": "action=buy&itemid=123"
        },
        {
          "type": "postback",
          "label": "Add to cart",
          "data": "action=add&itemid=123"
        },
        {
          "type": "uri",
          "label": "View detail",
          "uri": "https://ntplc.co.th"
        }
      ]
    }
  }

  return msg;
};
