/*
 * Imagemap Message
 * https://developers.line.biz/en/docs/messaging-api/message-types/#imagemap-messages
 * https://developers.line.biz/en/reference/messaging-api/#imagemap-message
 * https://developers.line.biz/en/reference/messaging-api/#base-url
 *
 */

exports.sendImagemap = () => {
  let msg;

  msg = {
    type: "imagemap",
    baseUrl: process.env.BASE_URL + "/images/static/imagemap",
    altText: "This is an imagemap",
    baseSize: {
      width: 1040,
      height: 1040,
    },
    video: {
      originalContentUrl: process.env.BASE_URL + "/images/static/imagemap/video.mp4",
      previewImageUrl: process.env.BASE_URL + "/images/static/imagemap/preview.jpg",
      area: {
        x: 0,
        y: 0,
        width: 1040,
        height: 585,
      },
      externalLink: {
        linkUri: "https://www.youtube.com/channel/UC4XxIa-1AUT3-01ZwdqbyUg",
        label: "ดูทั้งหมด",
      },
    },
    actions: [
      {
        type: "uri",
        linkUri: "https://ntplc.co.th/",
        area: {
          x: 0,
          y: 521,
          width: 520,
          height: 520,
        },
      },
      {
        type: "message",
        text: "map",
        area: {
          x: 521,
          y: 521,
          width: 520,
          height: 520,
        },
      },
    ],
  };

  return msg;
};
