const { client } = require("../config/line");
const path = require("path");
const { readFileSync } = require("fs");

/*
 * Rich Menu
 * https://developers.line.biz/en/docs/messaging-api/using-rich-menus/
 * https://developers.line.biz/en/docs/messaging-api/using-rich-menus/
 * https://developers.line.biz/en/docs/messaging-api/try-rich-menu/#actions-you-can-try-out-on-the-richmenu-playground
 * https://developers.line.biz/en/docs/messaging-api/try-rich-menu/#actions-you-can-try-out-on-the-richmenu-playground
 */

exports.createRichMenu = async () => {
  const richMenuAA = () => ({
    size: {
      width: 2500,
      height: 1686,
    },
    selected: false,
    name: "NT richmenu",
    chatBarText: "เมนูหลัก",
    areas: [
      {
        bounds: {
          x: 0,
          y: 0,
          width: 833,
          height: 843,
        },
        action: {
          type: "uri",
          uri: "https://google.com",
        },
      },
      {
        bounds: {
          x: 834,
          y: 0,
          width: 833,
          height: 843,
        },
        action: {
          type: "uri",
          uri: "https://facebook.com",
        },
      },
      {
        bounds: {
          x: 1666,
          y: 0,
          width: 833,
          height: 843,
        },
        action: {
          type: "message",
          text: "promotion"
        },
      },
      {
        bounds: {
          x: 0,
          y: 843,
          width: 833,
          height: 843,
        },
        action: {
          type: "uri",
          uri: "https://hotmail.com",
        },
      },
      {
        bounds: {
          x: 843,
          y: 843,
          width: 833,
          height: 843,
        },
        action: {
          type: "uri",
          uri: "https://ntplc.co.th",
        },
      },
      {
        bounds: {
          x: 1666,
          y: 843,
          width: 833,
          height: 843,
        },
        action: {
          type: "uri",
          uri: "https://google.com",
        },
      },
    ],
  });

  // create richmenu
  const richMenuAAId = await client.createRichMenu(richMenuAA());

  // upload image to richMenuAA
  const imagePath =
    path.resolve("./") + "/public/images/static/richmenu-aa.png";
  const bufferImage = readFileSync(imagePath);
  await client.setRichMenuImage(richMenuAAId, bufferImage);

  // set default menu to richMenuAA
  await client.setDefaultRichMenu(richMenuAAId);

  // create richmenu alias to richMenuAA
  await client.createRichMenuAlias(richMenuAAId, "richmenu-alias-aa");

};
