const { client } = require("../config/line");
const { sendText } = require("./send-text");
const { sendSticker } = require("./send-sticker");
const { sendLocation } = require("./send-location");
const { sendImage } = require("./send-image");
const { sendTemplate } = require("./send-template");
const { sendImagemap } = require("./send-imagemap");
const { sendFlexTemplate } = require("./send-flex-template");
const { sendCovidInfo } = require("./send-covid-info");
const { sendNetPromotions } = require("./send-net-promotions");

exports.handleMsg = async (event) => {
  let msg;

  // console.log(event.source.userId);

  switch (event.message.text.toLowerCase().trim()) {
    case "love":
      msg = sendSticker();
      break;
    case "map":
      msg = sendLocation();
      break;
    case "image":
      msg = sendImage();
      break;
    case "template":
      msg = sendTemplate();
      break;
    case "imagemap":
      msg = sendImagemap();
      break;
    case "flextemplate":
      msg = sendFlexTemplate();
      break;
    case "covid":
      msg = await sendCovidInfo();
      break;
    case "promotion":
      msg = await sendNetPromotions();
      break;
    default:
      msg = sendText(event);
      break;
  }

  return client.replyMessage(event.replyToken, msg);
};
