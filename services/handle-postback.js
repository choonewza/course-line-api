const { client } = require("../config/line");

// const querystring = require("node:querystring");

const { sendNetPromotionInfo } = require("./send-net-promotion-info");

exports.handlePostback = async (event) => {
  let msg;

  const postbackData = event.postback.data; 

  // const data = querystring.parse(event.postback.data);
  const data = JSON.parse(postbackData);

  switch (data.type) {
    case "promotion":
      msg = await sendNetPromotionInfo(data.id);
      break;
    default:
      console.log("Unkonw postback data", event.postback);
  }

  

  return client.replyMessage(event.replyToken, msg);
};
