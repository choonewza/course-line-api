// event handler
const {handleMsg} = require("./handle-msg");
const { handlePostback } = require("./handle-postback");

exports.handleEvent = async (event) => {
  // console.log(event);
  switch (event.type) {
    case "message":
      switch (event.message.type) {
        case "text":
        //   console.log("ลูกค้าส่งข้อความมา");
          return await handleMsg(event);
        case "sticker":
          console.log("ลูกค้าส่งสติ๊กเกอร์มา");
          break;
        default:
          throw new Error("Unknown message type");
      }
      break;
    case "follow":
      console.log("follow event type");
      break;
    case "postback":
      return await handlePostback(event);
    default:
      throw new Error("Unknown event type");
  }
};
