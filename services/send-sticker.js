/*
 * Sticker Message
 * https://developers.line.biz/en/reference/messaging-api/#sticker-message
 *
 * List of available stickers
 * https://developers.line.biz/en/docs/messaging-api/sticker-list/#sticker-definitions
 */

exports.sendSticker = (packageId = "11537", stickerId = "52002743") => {
  let msg;

  msg = {
    type: "sticker",
    packageId,
    stickerId,
  };

  return msg;
};
