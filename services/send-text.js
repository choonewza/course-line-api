exports.sendText = (event) => {
  let msg;
  let textMsg = event.message.text.toLowerCase().trim();

  if (textMsg === "hello") {
    msg = { type: "text", text: "สวัสดีครับ" };
  } else if (textMsg === "555") {
    msg = { type: "text", text: "ฮ่าๆๆๆ" };
  }else if (textMsg === "liff") {
    msg = { type: "text", text: "https://liff.line.me/1657320649-2KLr0Grm" };
  } else {
    msg = { type: "text", text: "สวัสดีครับ bot รายงานตัว" };
  }

  return msg;
};
