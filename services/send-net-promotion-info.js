/*
 * Image Message
 * https://developers.line.biz/en/reference/messaging-api/#image-message
 *
 */

const axios = require("axios").default;
const { numberFormat } = require("../libs/numeral");

const getNetPromotionById = async (id) => {
  const response = await axios.get(
    process.env.BASE_URL + "/api/promotion/" + id,
    {
      headers: { "Content-Type": "application/json" },
    }
  );
  const data = response.data;
  return data;
};

exports.sendNetPromotionInfo = async (id) => {
  let msg;

  const data = await getNetPromotionById(id);

  msg = {
    type: "flex",
    altText: "โปรโมชั่นเน็ต",
    contents: {
      type: "bubble",
      hero: {
        type: "image",
        url: data.picture,
        size: "full",
        aspectRatio: "20:13",
        aspectMode: "cover",
        action: {
          type: "uri",
          uri: "http://linecorp.com/",
        },
      },
      body: {
        type: "box",
        layout: "vertical",
        contents: [
          {
            type: "text",
            text: data.name,
            weight: "bold",
            size: "xl",
          },

          {
            type: "box",
            layout: "vertical",
            margin: "lg",
            spacing: "sm",
            contents: [
              {
                type: "box",
                layout: "baseline",
                spacing: "sm",
                contents: [
                  {
                    type: "text",
                    text: "ราคา",
                    color: "#000000",
                    size: "lg",
                    flex: 1,
                  },
                  {
                    type: "text",
                    text: numberFormat(data.price) + " บาท",
                    wrap: true,
                    color: "#666666",
                    size: "lg",
                    flex: 5,
                  },
                ],
              },
            ],
          },
        ],
      },
      footer: {
        type: "box",
        layout: "vertical",
        spacing: "sm",
        contents: [
          {
            type: "button",
            style: "link",
            height: "sm",
            action: {
              type: "uri",
              label: "CALL",
              uri: "https://linecorp.com",
            },
          },
          {
            type: "button",
            style: "link",
            height: "sm",
            action: {
              type: "uri",
              label: "WEBSITE",
              uri: "https://linecorp.com",
            },
          },
          {
            type: "box",
            layout: "vertical",
            contents: [],
            margin: "sm",
          },
        ],
        flex: 0,
      },
    },
  };

  return msg;
};
