/*
 * Location Message
 * https://developers.line.biz/en/reference/messaging-api/#location-message
 *
 */

exports.sendLocation = () => {
  let msg;

  msg = {
    type: "location",
    title: "บริษัท โทรคมนาคมแห่งชาติ จำกัด (มหาชน)",
    address: "99 ถนนแจ้งวัฒนะ แขวงทุ่งสองห้อง เขตหลักสี่ กรุงเทพ 10210",
    latitude: 13.8818957,
    longitude: 100.5690751,
  };
  return msg;
};
